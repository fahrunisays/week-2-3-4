@extends('layout.Master')

@section('judul')
Table
@endsection

@section('content')
<table class="table table-bordered" id="table1">
    <thead>                  
      <tr>
        <th style="width: 10px">#</th>
        <th>Country</th>
        <th>Total</th>
        <th style="width: 40px">Label</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1.</td>
        <td>Indonesia</td>
        <td>
          <div class="progress progress-xs">
            <div class="progress-bar progress-bar-danger" style="width: 55%"></div>
          </div>
        </td>
        <td><span class="badge bg-danger">55%</span></td>
      </tr>
      <tr>
        <td>2.</td>
        <td>Australia</td>
        <td>
          <div class="progress progress-xs">
            <div class="progress-bar bg-warning" style="width: 70%"></div>
          </div>
        </td>
        <td><span class="badge bg-warning">70%</span></td>
      </tr>
      <tr>
        <td>3.</td>
        <td>Singapore</td>
        <td>
          <div class="progress progress-xs progress-striped active">
            <div class="progress-bar bg-primary" style="width: 30%"></div>
          </div>
        </td>
        <td><span class="badge bg-primary">30%</span></td>
      </tr>
      <tr>
        <td>4.</td>
        <td>Others</td>
        <td>
          <div class="progress progress-xs progress-striped active">
            <div class="progress-bar bg-success" style="width: 5%"></div>
          </div>
        </td>
        <td><span class="badge bg-success">5%</span></td>
      </tr>
    </tbody>
  </table>
@endsection