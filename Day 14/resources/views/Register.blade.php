@extends('layout.Master')

@section('judul')
Sign Up Form
@endsection

@section('content')
    <b>Buat Account Baru!</b>
    <form action="/kirim" method="post">
        @csrf
        <label>First name:</label><br>
        <!input type="text">
        <input type="text" name="fname" id="fname" pattern="[A-Za-z]{2,}" title="Harap diawali huruf KAPITAL. Tuliskan 1 kata saja tanpa karakter khusus. Misal: Andriana"><br><br>

        <label>Last name:</label><br>
        <input type="text" name="lname" id="lname" pattern="[A-Za-z]{2,}" title="Harap diawali huruf KAPITAL. Tuliskan 1 kata saja tanpa karakter khusus. Misal: Stefie"><br><br>

        <label>Gender:</label><br>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Female<br>
        <input type="radio" name="gender">Other<br><br>

        <label>Nationality:</label><br>
        <select name="kwn">
            <option value="">Indonesian</option>
            <option value="">Singaporean</option>
            <option value="">Malaysian</option>
            <option value="">Australian</option>
        </select><br><br>

        <label>Language Spoken:</label><br>
        <input type="checkbox" name="bahasa" >Bahasa Indonesia<br>
        <input type="checkbox" name="bahasa">English<br>
        <input type="checkbox" name="bahasa">Other<br><br>        

        <label>Bio:</label><br>
        <textarea cols="80" rows="3"></textarea><br>

        <button type="submit">Sign Up</button>
    </form>
@endsection
