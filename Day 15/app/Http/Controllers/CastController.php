<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create(){
        return view('cast.tambah');
    }

    public function store(Request $request){
        $request->validate([
            'nama' =>'required|max:45',
            'umur' =>'required|max:2',
            'bio'  =>'required'
        ]);

        // buat masukkin inputan ke table dalam database

        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio'  => $request['bio']
        ]);
        
        return redirect('/cast');
    }
    // buat baca hasil masukan inputan dalam database & web localhost
    public function input(){
        $cast = DB::table('cast')->get();

        return view('cast.input', ['cast' => $cast]);
    }

    // buat nampilin si detail (bio)
    public function detail($id){
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('cast.detail', ['cast' => $cast]);
    }

    // fungsi buat nampilin edit
    public function edit($id){
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('cast.edit', ['cast' => $cast]);
    }

    // fungsi update si database
    public function update(Request $request, $id){
        $request->validate([
            'nama' =>'required|max:45',
            'umur' =>'required|max:2',
            'bio'  =>'required'
        ]);
        // syntax buat ngupdate
        DB::table('cast')
              ->where('id', $id)
              ->update(
                [
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio']
                ]
            );
        return redirect('/cast');
    }

    // Fungsi delete jalankan
    public function destroy($id){
        DB::table('cast')->where('id',$id)->delete();
        return redirect('/cast');
    }
    
}
