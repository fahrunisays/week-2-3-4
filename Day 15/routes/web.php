<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/base', [HomeController::class, 'home']);

Route::get('/register', [AuthController::class, 'form']);

// bikin route buat si proses request kirim formnya
Route::post('/kirim', [AuthController::class, 'kirim']);

Route::get('/welcome', [AuthController::class, 'welcome']);

Route::get('/table', function(){
    return view('table.Table');
});
Route::get('/data-tables', function(){
    return view('table.DataTable');
});

//CRUD Cast (Create, Read, Update, Delete)

// Create - Form Tambah Cast
Route::get('/', [CastController::class, 'create']);

// Create - kirim data ke database (biar bisa kebaca disana)
Route::post('/cast', [CastController::class, 'store']);

// Read - baca si data input dalam table di database
Route::get('/cast', [CastController::class, 'input']);

// Read - detail si bio nya
Route::get('/cast/{cast_id}', [CastController::class, 'detail']);

// Update - Form update cast
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);

// Update - update data ke database dari yang diedit masukin ke database
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

// Delete - berdasarkan id
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);
