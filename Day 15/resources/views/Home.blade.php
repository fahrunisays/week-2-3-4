@extends('layout.Master')

@section('judul')
SanberBook
@endsection

@section('content')
<b>SanberBook
Social Media Developer Santai Berkualitas</b>
    <p>Belajar dan Berbagi agar hidup ini semakin santai berkualitas</p>

    <b>Benefit Join di SanberBook</b>
    <ul>
        <li>Mendapatkan motivasi dari sesama developer</li>
        <li>Sharing knowledge dari para mastah Sanber</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul>

    <b>Cara Bergabung ke SanberBook</b>
    <ol>
        <li>Mengunjungi Website ini</li>
        <li>Mendaftar di <a href="/register">Form Sign Up</a> </li>
        <li>Selesai!</li>
    </ol>
@endsection
