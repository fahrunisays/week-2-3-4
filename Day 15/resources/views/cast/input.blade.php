@extends('layout.Master')

@section('judul')
Cast List
@endsection

@section('content')
<a href="/" class="btn btn-success btn-sm">Add List</a>
<br><br>
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">No</th>
        <th scope="col">Name</th>
        <th scope="col">Age</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
    @forelse ($cast as $key => $value)
        <tr>
            <td>{{$key+1}}</td>
            <td>{{$value->nama}}</td>
            <td>{{$value->umur}}</td>
            <td>
                {{-- <a href="/cast/{cast_id} bagian ini arahin ke $valuenya ...  --}}
                {{-- Biar rapi si detail & edit masukin ke form biar jadi 1 dimensi --}}
                <form action="/cast/{{$value->id}}" method="post">
                    @csrf {{-- setiap ada form dikasi CSRF --}}
                    @method('delete') {{-- Mirip sama methode put tadi --}}
                    <a href="/cast/{{$value->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/cast/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
            </td>
        </tr>
    @empty
        <tr><td>No Data</td></tr>
    @endforelse
    </tbody>
  </table>
  

@endsection
