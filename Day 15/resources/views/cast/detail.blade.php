@extends('layout.Master')

@section('judul')
Details bio   
@endsection

@section('content')
<h3><b>{{$cast->nama}} ({{$cast->umur}} Years Old)</b></h3>
<p>{{$cast->bio}}</p>

<a href="/cast" class="btn btn-secondary btn-sm">Back to list</a>

@endsection