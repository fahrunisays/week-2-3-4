@extends('layout.Master')

@section('judul')
Add Cast
@endsection

@section('content')
<form action="/cast" method="post">
    @csrf
    <div class="form-group">
        <label>Cast Name</label>
        <input type="text" name="nama" class="form-control">
    </div>
    {{-- tampilan error --}}
    @error('nama')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
        <label>Age</label>
        <input type="number" name="umur" class="form-control">
    </div>
    {{-- tampilan error --}}
    @error('umur')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
        <label>Bio</label>
        <textarea name="bio" class="form-control" cols="30" rows="5" ></textarea>
    </div>
    {{-- tampilan error --}}
    @error('bio')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <button type="submit" class="btn btn-success">Submit</button>
</form>
@endsection