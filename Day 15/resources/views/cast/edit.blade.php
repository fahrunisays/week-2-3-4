@extends('layout.Master')

@section('judul')
Edit Cast
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('put')

    <div class="form-group">
        <label>Cast Name</label>
        <input type="text" name="nama" value="{{$cast->nama}}" class="form-control">
    </div>
    {{-- tampilan error --}}
    @error('nama')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
        <label>Age</label>
        <input type="number" name="umur" value="{{$cast->umur}}" class="form-control">
    </div>
    {{-- tampilan error --}}
    @error('umur')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
        <label>Bio</label>
        <textarea name="bio" class="form-control" cols="30" rows="5" >{{$cast->bio}}</textarea>
    </div>
    {{-- tampilan error --}}
    @error('bio')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="d-flex justify-content-between">
    <a href="/cast" class="btn btn-secondary">Cancel</a>
    <button type="submit" class="btn btn-success">Submit</button>
    </div>
</form>

@endsection