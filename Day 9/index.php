<?php

require_once("animal.php");
require_once("frog.php");
require_once("ape.php");

$sheep = new animal("Shaun");

echo "Name : " . $sheep->nama . "<br>";
echo "Legs : " . $sheep->legs . "<br>";
echo "Cold Blooded : " . $sheep->cold_blooded . "<br><br>";

$frog = new katak("Buduk");
echo "Name : " . $frog->nama . "<br>";
echo "Legs : " . $frog->legs . "<br>";
echo "Cold Blooded : " . $frog->cold_blooded . "<br>";
echo "Jump : ".$frog->jump()."<br><br>" ; 


$ape = new primata("Kera Sakti");
echo "Name : " . $ape->nama . "<br>";
echo "Legs : " . $ape->legs . "<br>";
echo "Cold Blooded : " . $ape->cold_blooded . "<br>";
echo "Yell : ".$ape->yell()."<br><br>" ; 


?>

<!-- // index.php -->
<!-- $sungokong = new Ape("kera sakti");
$sungokong->yell() // "Auooo"

$kodok = new Frog("buduk");
$kodok->jump() ; // "hop hop" -->