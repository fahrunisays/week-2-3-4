<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/kirim" method="post">
        @csrf
        <label>First name:</label><br><br>
        <!input type="text">
        <input type="text" name="fname" id="fname" pattern="[A-Za-z]{2,}" title="Harap diawali huruf KAPITAL. Tuliskan 1 kata saja tanpa karakter khusus. Misal: Andriana"><br><br>

        <label>Last name:</label><br><br>
        <input type="text" name="lname" id="lname" pattern="[A-Za-z]{2,}" title="Harap diawali huruf KAPITAL. Tuliskan 1 kata saja tanpa karakter khusus. Misal: Stefie"><br><br>

        <label>Gender:</label><br><br>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Female<br>
        <input type="radio" name="gender">Other<br><br>

        <label>Nationality:</label><br><br>
        <select name="kwn">
            <option value="">Indonesian</option>
            <option value="">Singaporean</option>
            <option value="">Malaysian</option>
            <option value="">Australian</option>
        </select><br><br>

        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="bahasa" >Bahasa Indonesia<br>
        <input type="checkbox" name="bahasa">English<br>
        <input type="checkbox" name="bahasa">Other<br><br>        

        <label>Bio:</label><br><br>
        <textarea cols="35" rows="10"></textarea><br>

        <button type="submit">Sign Up</button>
    </form>

</body>
</html>